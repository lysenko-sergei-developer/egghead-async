async function fetchFromGitHub (handle) {
  const url = `https://api.github.com/users/${handle}`
  const response = await fetch(url)
  const body = await response.json()


  if (response.status !== 200) 
    throw Error(body.message)

  return body
}

async function showGitHubUser (handle) {
  try {
    const user = await fetchGitHUbUser(handle)
    console.log(user.name)
    console.log(user.location)
  } catch (err) {
    console.error(`Error: ${err.message}`)
  }
}

showGitHubUser("someuser")