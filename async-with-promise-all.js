async function fetchFromGitHub (handle) {
  const url = `https://api.github.com/users/${handle}`
  const response = await fetch(url)
  return await response.json()
}

async function showUserAndRepos (handle) {
  const [user, repos] = Promise.all ([
    fetchFromGitHub(`/users/${handle}`),
    fetchFromGitHub(`/users/${handle}/repos`)
  ])

  const user = result[0]
  const repos = result[1]

  console.log(user.name)
  console.log(`${repos.length} repos`)
}

showUserAndRepos("someuser")